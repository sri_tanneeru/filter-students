=begin
Implement the filter method. It should accept a list of students and a variable number of filters. The filters will be passed in as lambdas, each of which will take one student record and return a boolean value.

filter should return a list of student records that have passed through all the filters.

If no filters are provided, the list of student records should be returned as is. If no students pass through all the provided filters (or if no students are provided), return an empty array.

For example, consider the following code:

students = [
  { name: 'Thomas Edison', gpa: 3.45 },
  { name: 'Grace Hopper', gpa: 3.99 },
  { name: 'Leonardo DaVinci', gpa: 2.78 }
]

honor_roll = ->(record) { record[:gpa] > 3.0 }



filter(students, honor_roll) should return:

[{:name=>"Thomas Edison", :gpa=>3.45},{:name=>"Grace Hopper", :gpa=>3.99}]

=end

def filter_method(students: [], block: nil)
	return [] if ((students.empty? && block.nil?)|| students.empty?)
	return students if block.nil?
	return students.select{|i| block.call(i)}
end

students = [
  { name: 'Thomas Edison', gpa: 3.45 },
  { name: 'Grace Hopper', gpa: 3.99 },
  { name: 'Leonardo DaVinci', gpa: 2.78 }
]

h = ->(record) { record[:gpa] > 3.0 }

puts "Test case with arguments : #{filter_method(students: students, block: h)}"

puts "Test case with no arguments : #{filter_method()}"

puts "Test case with only students given not filters provided : #{filter_method(students: students)}"

puts "Test case with no students and filters given : #{filter_method(block: h)}"

